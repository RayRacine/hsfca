{-
     FCA - A generator of a Formal Concept Analysis Lattice
     Copyright (C) 2014  Raymond Racine

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU Affero General Public License as
     published by the Free Software Foundation, either version 3 of the
     License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU Affero General Public License for more details.

     You should have received a copy of the GNU Affero General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE ImpredicativeTypes   #-}
{-# LANGUAGE Rank2Types           #-}
{-# LANGUAGE TypeSynonymInstances #-}

{-# LANGUAGE FlexibleInstances    #-}

module Data.Fca.Ident (
  Ident,
  Identable (..)
) where

import           Crypto.Hash          (Digest, MD5, hash)
import qualified Data.HashSet         as Set
import           Data.Text            (Text)
import qualified Data.Text            as T
import           Data.Text.Encoding   (encodeUtf8)

import           Data.Fca.Concept     (Concept, cG)
import           Data.Fca.SimpleTypes (Obj)


type Ident = Text

class Identable a where
  ident :: a -> Text

instance Identable (Concept (Obj Text) a) where
  ident c = hmd5
    where
      bs = encodeUtf8 . T.concat . Set.toList . cG $ c
      hmd5 = T.pack $ show (hash bs :: Digest MD5)

