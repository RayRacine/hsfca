{-
     FCA - A generator of a Formal Concept Analysis Lattice
     Copyright (C) 2014  Raymond Racine

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU Affero General Public License as
     published by the Free Software Foundation, either version 3 of the
     License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU Affero General Public License for more details.

     You should have received a copy of the GNU Affero General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module Data.Fca.SimpleTypes  where

import           Data.Fca.CElem      ()

import           Data.HashMap.Strict (HashMap)
import           Data.HashSet        (HashSet)

type Map k v = HashMap k v
type Set a = HashSet a

type Obj  o = o
type Attr a = a
type G o    = Set (Obj o)
type M a    = Set (Attr a)
type I o a  = [(Obj o, Attr a)]

data Context o a = Context { ctxG :: G o,
                             ctxM :: M a,
                             ctxI :: I o a }

type IdxGM o a = Map (Obj o) (M a)

type IdxMG a o = Map (Attr a) (G o)
