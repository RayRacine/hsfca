{-# LANGUAGE FlexibleContexts #-}

{-
     FCA - A generator of a Formal Concept Analysis Lattice
     Copyright (C) 2014  Raymond Racine

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU Affero General Public License as
     published by the Free Software Foundation, either version 3 of the
     License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU Affero General Public License for more details.

     You should have received a copy of the GNU Affero General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module Data.Fca.Cid (
  Cid (..),
  cid
) where

import           Data.Hashable    (Hashable (hashWithSalt))
import           Data.Text        (Text)

import           Data.Fca.Concept (Concept)
import           Data.Fca.Ident   (Identable (ident))


newtype Cid = Cid Text
        deriving (Eq, Show)

instance Hashable Cid where
  hashWithSalt salt (Cid c) = hashWithSalt salt c

cid :: Identable (Concept o a) =>  Concept o a -> Cid
cid c = Cid $ ident c
