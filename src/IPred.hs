module IPred
where

import Data.List

-- -----------------------------------------------------------------
-- Implementation of the iPred algorithm for computing the Hasse diagram of a concept lattice 
-- Detailed in ICFCA 2009: Yet a Faster Algorithm for Building the Hasse Diagram of a Concept Lattice

-- cs is the list of formal concepts
-- ds is the set delta
-- ls represents the set of elements of the lattice
-- bs is the border

mySort :: (Ord a) => [[a]] -> [[a]]
mySort [] = []
mySort (x:xs) = mySort left ++ [x] ++ mySort right
    where left  = [y | y <-xs, length y <= length x]
          right = [y | y<-xs, length y > length x]

generateHasse xs = 
    let cs = mySort (map (concat) xs)
        ds = [("",[])] ++ [(x,[]) | x<-cs]
    in mainLoop ([""] ++ cs) ds [] []

mainLoop [] ds ls bs = ls
mainLoop (c:cs) ds ls bs = processCandidate (c:cs) ds ls bs (candidate c bs)

processCandidate (c:cs) ds ls bs [] = mainLoop cs ds ls (bs ++ [c])
processCandidate (c:cs) ds ls bs (k:ks) 
    | emptyIntersection ds c k = processCandidate (c:cs) (updateDelta ds c k [])  ((k,c):ls) (bs \\ [k]) ks
    | otherwise = processCandidate (c:cs) ds ls bs ks

emptyIntersection :: (Eq a) => [([a], [[a]])] -> [a] -> [a] -> Bool
emptyIntersection ds c k 
    | or (map (\y -> myIntersect c y) (getConceptsOfDeltaK ds k)) = False
    | otherwise = True 

myIntersect c [] = False
myIntersect c ds 
    | intersect c ds /= [] = True
    | otherwise = False	

getConceptsOfDeltaK :: Eq a => [([a], [[a]])] -> [a] -> [[a]]
getConceptsOfDeltaK [] k = []
getConceptsOfDeltaK (d:ds) k
    | (fst d) == k = snd d
    | otherwise = getConceptsOfDeltaK  ds k

updateDelta [] c k ds' = ds'
updateDelta (d:ds) c k ds' 
    | (fst d)==k = ds' ++ [(fst d, (snd d) ++ ((c \\ k):[]) )] ++ ds
    | otherwise = updateDelta ds c k (d : ds')

candidate :: (Eq a) => [a] -> [[a]] -> [[a]]
candidate c bs = nub [intersect c b | b<-bs]
