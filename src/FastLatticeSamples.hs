module FastLatticeSamples
where

import FastLattice

-- Sample call

sampleLattice :: IO ()
sampleLattice = do
  print $ generateLattice (Context g1 m1 i1)
  print $ generateLattice (Context g1' m1' i1')

-- Examples for Lattice

g1 = ["1","2","3","4","5"]
m1 = ["a","b","c","d","e"]
i1 = [("1","a"),("1","b"),("1","c"),("2","a"),("2","c"),("3","a"),("3","c"),("3","d"),("4","a"),("4","b"),("4","c"),("5","b"),("5","d")]


g1' = ["1","2","3","4"]
m1' = ["a","b","c","d","e"]
i1' = [("1","a"),("1","b"),("1","d"),("2","b"),("2","c"),("3","a"),("3","c"),("3","d"),("3","e"),("4","b"),("4","d")]

-- ----------------------
-- iPred Hasse generation
-- ----------------------

-- To run iPred only, compute (start ganter) or the following main function 

main2 = print (generateHasse ganter)

-- To compute the Hasse diagram on the formal concepts calculater by Lattice, run:

-- main3 = print (start [ snd x | x<- (lattice (groupObj g1 i1) (groupAttr m1 i1) g1 )])

-- main4 = print (start [ snd x | x<- (lattice (groupObj myTi myTd) (groupAttr myTa myTd) myTi )])

-- main5 = print (start [ snd x | x<- (lattice (groupObj myTi myTd3) (groupAttr myTa myTd3) myTi )])

-- Examples for iPred

ltest = [["c","d","e"],["d","e"],["a","b","c"],["b","c","d"],["c"],["b","c"],["d"],["c","d"],["a","d","e"],["a"],["a","b","c","d","e"]]

ganter = [["a","c","d","e"],["a","b","d"],["d"],["b","d"],["b","c"],["c"],["b"],["a","d"],["a","b","c","d","e"]]

showGMF ls = "graph [ "++ showGMFEdges ls ++ " ]"
showGMFEdges [] = ""
showGMFEdges (l:ls) = "edge [ source \""++ fst l ++ "\" target \"" ++ snd l ++"\" value 1 ]" ++ showGMFEdges ls

