module FastLattice (
  Context (..),
  generateLattice,
  lattice
) where

import           Data.List            (intersect, nub, (\\))
import           Prelude              hiding (min)
-- import           Debug.Trace

import           Data.Fca.SimpleTypes (Attr, Obj)

-- ------------------------------------------------------------------------------
-- Implementation of the LATTICE algo of Christian Lindig (Fast Concept Analysis)
-- ------------------------------------------------------------------------------

data Context o a = Context {
      ctxGs :: [o],
      ctxMs :: [a],
      ctxIs :: [(o,a)]
    } deriving Show

groupObj :: Eq o => [o] -> [(o, a)] -> [(o, [a])]
groupObj oos is = gObj oos []
  where
   gObj [] rs = rs
   gObj (o:os) rs = gObj os ([(o, [snd i|i<-is, (fst i) == o])] ++ rs)

groupAttr :: Eq a => [a] -> [(o,a)] -> [(a,[o])]
groupAttr aas is = gAttr aas []
  where
    gAttr [] rs = rs
    gAttr (a:as) rs = gAttr as ([(a, [fst i|i<-is, (snd i) == a])] ++ rs)

setIntersect :: Eq t => [[t]] -> [t]
setIntersect [] = []
setIntersect (x:[]) = x
setIntersect (x:xs) = intersect x (setIntersect xs)

gPrime :: (Eq o, Eq a) => [o] -> [(o,[a])] -> [a]
gPrime os gs = setIntersect [snd g | g <- gs, o <- os, (fst g) == o]

-- -- neighbors objectsMap attributesMap ConceptToComputNeighborsFor
neighbors :: (Show o, Show a, Eq o, Eq a) => [(o,[a])] -> [(a,[o])] -> [o] -> [o] ->[([o],[a])] -> [o] -> [([o],[a])]
neighbors _ _ _ _ ns [] = ns
neighbors gos aos igs min ns (gg:ggs)
     | intersect min (tmpG1 \\ (nub (igs ++ [gg]))) == [] =
        neighbors gos aos igs min (ns ++ [(tmpG1, tmpM1)]) ggs
     | otherwise = neighbors gos aos igs (min \\ [gg]) ns ggs
  where tmpM1 = gPrime (igs ++ [gg]) gos
        tmpG1 = gPrime tmpM1 aos

-- --loopLattice :: (Eq (Obj o), Eq (Attr a)) => [ObjAttrs o a] -> [AttrObjs o a] -> Objs o -> OIAs -> OIAs -> OIAs
loopLattice :: (Show o, Show a, Eq a, Eq o) => [(o, [a])] -> [(a, [o])] -> [o] -> [([o], [a])] -> [([o], [a])] ->
                                                [([o], [a])]
loopLattice _ _ _ [] rs = rs
loopLattice gos aos gs (n:ns) rs =
     loopLattice gos aos gs
                 ((nub (ns ++ (neighbors gos aos (fst n) (gs \\ (fst n)) [] (gs \\ (fst n))) )) \\ [([],[])]) (rs ++ [n])

lattice :: (Show o, Show a, Eq a, Eq o) => [(o, [a])] -> [(a, [o])] -> [o] -> [([o], [a])]
lattice gos aos gs = loopLattice gos aos gs ns []
  where ns = (neighbors gos aos [] gs [] gs)

generateLattice :: (Show o, Show a, Eq a, Eq o) =>  Context o a -> [([Obj o], [Attr a])]
generateLattice (Context gs ms is) = lattice ggs gms gs
  where
    ggs = groupObj gs is
    gms = groupAttr ms is

