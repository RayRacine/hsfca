Lindig's FastLattice Algo for Formal Concept Analysis

```

{-# LANGUAGE OverloadedStrings #-}


import qualified Data.Fca     as SFL (Context (..), generateLattice)
import qualified FastLattice  as FL

import           Data.HashSet (fromList)
import qualified Data.List    as L (length)
import           Data.Text

type Ident = Text

objs1 :: [Ident]
objs1 = ["Leech", "Bream", "Frog", "Dog", "Spike", "Reed", "Bean", "Maize"]

attrs1 :: [Ident]
attrs1 =  ["water", "inwater", "moves", "limbs", "onland",
           "suckles", "chloro", "leavesone", "leavestwo"]

rels1 :: [(Ident, Ident)]
rels1 = [ ("Leech", "water"), ("Leech", "inwater"), ("Leech", "moves" ) ,
          ("Bream", "water"), ("Bream", "inwater"), ("Bream", "moves"), ("Bream", "limbs"),
          ("Frog", "water"), ("Frog", "inwater"), ("Frog", "onland"), ("Frog", "moves"),  ("Frog", "limbs"),
    ("Dog", "water"), ("Dog", "onland"), ("Dog", "moves"), ("Dog", "limbs"), ("Dog", "suckles"),
    ("Spike", "water"), ("Spike", "inwater"), ("Spike", "chloro"),  ("Spike", "leavesone"),
    ("Reed", "water"), ("Reed", "inwater"), ("Reed", "onland"), ("Reed", "chloro"), ("Reed", "leavesone"),
    ("Bean", "water"), ("Bean", "onland") , ("Bean", "chloro"),  ("Bean", "leavestwo"),
    ("Maize", "water"), ("Maize", "onland"), ("Maize", "chloro"),  ("Maize", "leavesone")]

ctx1 :: SFL.Context Ident Ident
ctx1 = SFL.Context (fromList objs1) (fromList attrs1) rels1

main :: IO ()
main = do
  putStrLn "Here we go"
  -- let cs  = FL.generateLattice ctx2
  let (l, cs) = SFL.generateLattice ctx1
  putStrLn "========================"
  putStrLn (show cs)
  putStrLn "++++++++++++++++++++++++"
  putStrLn $ show l

```